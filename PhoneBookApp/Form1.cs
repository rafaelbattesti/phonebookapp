﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

//Same namespace contains both view (Form1.Designer.cs) and controller (Form1.cs)
namespace PhoneBookApp
{
    public partial class Form1 : Form
    {
        //Constants
        string DELIMITER = ",";

        //Data fields
        string path = "phonebook.txt";
        int index = -1;

        //Constructor calls InitializeComponent() from view
        public Form1()
        {
            InitializeComponent();
        }

        //On load method must read the file
        private void Form1_Load(object sender, EventArgs e)
        {
            //Adds the method to the event property of the object
            txtName.KeyPress += txtName_KeyPress;
            txtPNum.KeyPress += txtPNum_KeyPress;
            
            //Sets the context menu to empty
            txtName.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            txtPNum.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();

            //If file exists
            if (File.Exists(path))
            {
                readFile();
            }
        }

        //Acts when an item from the list box is selected
        private void lstDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Grabs the item
            index = lstDisplay.SelectedIndex;

            if (index > -1)
            {
                //Get the record
                string record = lstDisplay.Items[index].ToString();

                //Tokenize the fields
                char[] delim = { ',' };
                string[] tokens = record.Split(delim[0]);

                //Assign to each textfield
                if (tokens.Length == 2)
                {
                    txtName.Text = tokens[0];
                    txtPNum.Text = tokens[1];
                    setControlState("u/d");
                }
                else
                {
                    MessageBox.Show("Missing field delimiter in record!", "Invalid File Structure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }  
            }
        }

        //Reads the file into the list box Items collection
        private void readFile()
        {
            //Empty the list box Items collection
            lstDisplay.Items.Clear();

            //Read line by line until null
            using (StreamReader sr = new StreamReader(path))
            {
                string record = "";
                while ((record = sr.ReadLine()) != null)
                {
                    lstDisplay.Items.Add(record);
                }
            }
        }

        //Inserts record after validation
        private void cmdInsert_Click(object sender, EventArgs e)
        {
            string record = txtName.Text + DELIMITER + txtPNum.Text;

            if (cmdInsert.Text.Equals("Return to Insert Mode"))
            {
                setControlState("i");
                return;
            }

            if (dataGood())
            {
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.WriteLine(record);
                    clearText();
                }
                if (File.Exists(path))
                {
                    readFile();
                }
            }
        }

        //Method to update the record. Deletes entire record and rewrites it.
        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (dataGood())
            {
                //Remove previous file
                File.Delete(path);
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    //Write all elements
                    for (int i = 0; i < lstDisplay.Items.Count; i++)
                    {
                        if (i != index) //write from listbox
                        {
                            sw.WriteLine(lstDisplay.Items[i].ToString());
                        }
                        else //write from textboxes
                        {
                            sw.WriteLine(txtName.Text + DELIMITER + txtPNum.Text);
                        }
                    }
                }
                readFile();
                setControlState("i");
            }
        }

        //Deletes a record
        private void cmdDelete_Click(object sender, EventArgs e)
        {
            //Opens the dialog box and compares with a yes
            if (MessageBox.Show("Are you sure you want to delete this record?", "Confirm Record Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                File.Delete(path);
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    for (int i = 0; i < lstDisplay.Items.Count; i++)
                    {
                        if (i != index) //write from listbox
                        {
                            sw.WriteLine(lstDisplay.Items[i].ToString());
                        }
                    }
                }
                readFile();
            }
            setControlState("i");
        }

        //Sets the control state for buttons
        private void setControlState(string state)
        {
            if (state.Equals("i"))
            {
                cmdInsert.Text = "Insert Record";
                cmdDelete.Enabled = false;
                cmdUpdate.Enabled = false;
                clearText();
            }
            else if (state.Equals("u/d"))
            {
                cmdDelete.Enabled = true;
                cmdUpdate.Enabled = true;
                cmdInsert.Text = "Return to Insert Mode";
            }
        }

        //Method to validate for empty or 
        private bool dataGood()
        {
            //Criteria for name field
            if (txtName.Text.Length < 1)
            {
                //TODO
                MessageBox.Show("First name required!", "Missing Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();
                return false;
            }
            if (txtName.Text.Length == 1)
            {
                //TODO
                MessageBox.Show("First name must contain 2 or more characters!", "Invalid First Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();
                return false;
            }
            if (txtName.Text.Length > 1)
            {
                //TODO
                if (txtName.Text.IndexOf(" ") == -1 || txtName.Text.Length == txtName.Text.IndexOf(" ") + 1)
                {
                    MessageBox.Show("Last name required!", "Missing Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtName.Focus();
                    return false;
                }
                if (txtName.Text.Length == txtName.Text.IndexOf(" ") + 2)
                {
                    MessageBox.Show("Last name must contain 2 or more characters!", "Invalid Last Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtName.Focus();
                    return false;
                }

            }

            //Validate Phone Number
            if (txtPNum.Text.Length < 1)
            {
                MessageBox.Show("Phone number required!", "Missing Phone Number", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPNum.Focus();
                return false;
            }
            if (txtPNum.Text.Length != 12)
            {
                MessageBox.Show("Phone number must be ###-###-####!", "Invalid Phone Number", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPNum.Focus();
                return false;
            }
            return true;
        }

        //Method to clear data
        private void clearText()
        {
            txtName.Text = "";
            txtPNum.Text = "";
            txtName.Focus();
            lstDisplay.ClearSelected();
        }

        //Method to validate user input
        /*
         * Nasty values:
         * Backspace = 8
         * 0-9 = 48-57
         * dash = 45
         */
        void txtPNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Capture the key (KeyChar is a property)
            char c = e.KeyChar;
            int len = txtPNum.Text.Length;
            txtPNum.SelectionStart = len;

            //If not a backspace
            if (c != 8)
            {
                //If dash position
                if (len == 3 || len == 7)
                {
                    //If not dash
                    if (c != 45)
                    {
                        //Do not deliver the keypress
                        e.Handled = true;
                    }
                }
                else
                {
                    //Numeric digit only
                    if (c < 48 || c > 57)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        //Method to validate user input
        /*
         * Nasty values:
         * a-z = 97-122
         * A-Z = 65-90
         * space = 33
         */
        void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Capture the keychar
            char c = e.KeyChar;
            int len = ((TextBox)sender).Text.Length;
            ((TextBox)sender).SelectionStart = len;

            if (c != 8)
            {
                //Not alpha or space
                if ((c < 65 || c > 90) && (c < 97 || c > 122) && (c != 32))
                {
                    e.Handled = true;
                }

                //If it is smaller than 2 chars
                if (len < 2)
                {
                    //If space, kill it
                    if (c == 32)
                    {
                        e.Handled = true;
                    }
                    else if (len == 0 && (c > 96 && c < 123))
                    {
                        e.KeyChar = (char)(c - 32);
                    }
                    else if (len > 0 && (c > 64 && c < 91))
                    {
                        e.KeyChar = (char)(c + 32);
                    }
                }
                else
                {
                    //If there is no space in the current value in the textbox
                    if (((TextBox)sender).Text.IndexOf(" ") == -1)
                    {
                        //No space...char for first name
                        if (c > 64 && c < 91)
                        {
                            e.KeyChar = (char)(c + 32);
                        }
                    }
                    else if (c == 32)
                    {
                        e.Handled = true;
                    }
                    else if (((TextBox)sender).Text.IndexOf(" ") == len - 1)
                    {
                        //Space is the last char..first letter for the last name
                        if (c > 96 && c < 123)
                        {
                            e.KeyChar = (char)(c - 32);
                        }
                    }
                    else if (((TextBox)sender).Text.IndexOf(" ") < len - 1)
                    {
                        if (c > 64 && c < 91)
                        {
                            e.KeyChar = (char)(c + 32);
                        }
                    }
                }
            }
        }
    }
}