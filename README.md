# README #

This is a simple phone book application for educational purposes. The main purpose of this practice is the application of C# concepts in Windows Form Applications and exercise Git branching. Git branching is defined as a main branch, from which the developer forked new ones for each specific implementation. Please visit the branches section for more details.

### What is this repository for? ###

* PhoneBookApp stores contact information in a sequential text file, implementing CRUD. In the future, a good exercise is implementing a data access layer in this application and a model.
* Version: 0.1
* [Learn Markdown](https://bitbucket.org/rafaelbattesti/phonebookapp)

### How do I get set up? ###

* Summary of set up: The application runs as a standalone windows app.
* Configuration: It runs on Windows using the .NET framework version 4.5.
* Dependencies: N/A
* Database configuration: N/A
* How to run tests: Not yet developed, as the course does not have a TDD approach to development.
* Deployment instructions: Not yet defined.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [rafael.battesti@gmail.com](mailto:rafael.battesti@gmail.com)